#ifndef __PGM_H
#define __PGM_H

typedef struct PGM {
    int width; //largeur
    int height; //hauteur
    int max_gray; //valeur maximale des niveaux de gris
    int **pixel; //tableau des valeurs des pixels
} PGM;

PGM getPGMFromFile(const char *file_name);

#endif