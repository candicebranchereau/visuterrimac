#ifndef QUADTREE_H
#define QUADTREE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "geometry.h"
#include "colors.h"
#include "pgm.h"

typedef struct QuadTree {

    //les 4 noeuds enfants
    struct QuadTree* topLeft;
    struct QuadTree* topRight;
    struct QuadTree* bottomLeft;
    struct QuadTree* bottomRight;

    RectangleSpace boundaries; //délimitation de l'espace

    Vector3D vertex[4]; //Liste des 4 points

    int level; //profondeur du noeud
    ColorRGB color;

} QuadTree;

QuadTree initQuadTree(RectangleSpace boundaries, int level, ColorRGB color);
void subdiviseSpace(QuadTree* quadTree, PGM* pgm);

bool isLeaf(QuadTree* quadTree);
int getHeight(QuadTree* quadTree);

#endif