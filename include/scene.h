#ifndef __SCENE_H
#define __SCENE_H

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include <math.h>

#include "map.h"
#include "geometry.h"
#include "colors.h"
#include "camera.h"

#define MAX_TREE_COUNT 50

typedef struct Tree {
    float size_x;
    float size_y; 
    Vector3D pos;
    GLuint texture;
    //char* texture_file_name;
} Tree;


typedef struct Light {
    Vector3D pos;
    ColorRGB color;
    float radius;
} Light;


typedef struct Skybox {
    float size;
    GLuint texture;
} Skybox;


typedef struct Scene {
    Map map;
    Light sun;
    Skybox skybox;
    Light light;
    Tree trees[MAX_TREE_COUNT];
    int treeCount;

    Camera camera;

    int z_near; //plan eloigné de la caméra
    int z_far; //plan proche de la caméra
    int fov; //angle visible par la camera
} Scene;


Scene createScene(const char *config_name, Skybox skybox, Light light, Camera camera);
void readFileData(const char *config_name, Scene *scene);

Tree createTree(float size_x, float size_y, Vector3D pos, char* texture_file_name);
void addTreeToScene(Scene* scene, Tree tree);

Light createLight(Vector3D pos, ColorRGB color, float radius);
void moveLight(Light *light, float thetalight);
ColorRGB modelLambert(ColorRGB color, Vector3D normale, Light sun);

Skybox createSkybox(float size);


#endif