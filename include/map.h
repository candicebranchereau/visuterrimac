#ifndef MAP_H
#define MAP_H

#include <GL/gl.h>
#include "pgm.h"
#include "geometry.h"
#include "quadTree.h"
#include "colors.h"

#define NbNodes_MAX 10000

typedef struct Map {
    QuadTree quadTree;
    PGM pgm; //données du fichier .pgm
    
    int x_size; //longueur totale du terrain
    int y_size; //largeur totale du terrain
    int z_min; //hauteur minimale du terrain
    int z_max; //hauteur maximale du terrain
} Map;

Map createMap(const char *file_name, int x_size, int y_size, int z_min, int z_max);
void fillQuadTree(Map* map);

#endif