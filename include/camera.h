#ifndef __CAMERA_H
#define __CAMERA_H

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "geometry.h"

#include <math.h>

typedef struct Camera {
    Vector3D pos;
    Vector3D target;
    Vector3D up;
    float angle;
} Camera;

Camera createCamera(Vector3D pos);
void moveCamera(Camera* camera, float theta, float phi);
void translateLeft(Camera* camera, float step);
void translateRight(Camera* camera, float step);
void translateUp(Camera* camera, float step);
void translateDown(Camera* camera, float step);
void translateFront(Camera* camera, float step, float theta, float phi);
void translateBack(Camera* camera, float step, float theta, float phi);

#endif