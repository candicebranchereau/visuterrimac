#ifndef __DRAW_H
#define __DRAW_H

#include <GL/gl.h>

#include "scene.h"

// Fonction de dessin d'un repere
void glDrawRepere(float length, bool display);

void drawScene(Scene scene, bool display);
void drawTree(Scene scene, Tree tree);
void drawLight(Light light);
void drawSkybox(Scene scene, Skybox skybox);
void drawMap(Map map,QuadTree* quadTree, int level, Vector3D camera_pos, Light light, bool fill);

#endif