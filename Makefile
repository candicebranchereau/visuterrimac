# $(BIN) est le nom du binaire généré
BIN = visu
# FLAG
FLAGS = -Wall -O3
# INCLUDES
#INC = -I include
# INCLUDES
LIBS = 
# INCLUDES
LIBDIR = -lSDL2 -lglut -lGLU -lGL -lm 
# Compilateur
GCC = gcc
# $(OBJECTS) sont les objets qui seront générés après la compilation
OBJECTS = gldrawing.o visu.o create_object.o

BIN_DIR	= bin
INC_DIR = -I include
SRC_DIR	= src
OBJ_DIR	= obj

SRC_FILES 	= $(shell find $(SRC_DIR)/ -type f -name '*.c')
OBJ_FILES 	= $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o, $(SRC_FILES))
EXEC_BIN	= visu


default: $(BIN)

all: $(OBJ_FILES)

#$(BIN): $(OBJECTS)
#	@echo "**** PHASE DE LIEN ****"
#	$(GCC) $(OBJECTS) $(FLAGS) -o $(BIN) $(LIBDIR) $(LIBS) 

visu : $(OBJ_FILES)
	@mkdir -p $(BIN_DIR)/
	$(CC) -o $(BIN_DIR)/$(EXEC_BIN) $(OBJ_FILES) $(LIBDIR) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p "$(@D)"
	$(CC) -c $< -o $@ $(FLAGS) $(INC_DIR)

clean:
	rm -rf *~
	rm -rf $(SRC_DIR)/*/*~
	rm -rf $(OBJ_DIR)/
	rm -rf $(BIN_DIR)/*
