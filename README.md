# VisuTerrImac

## Guide de Plim

__Modes__
- F : permet de passer au mode filaire
- P : permet de passer en mode plein

__Positions__
- S : rotation de la caméra vers le bas
- Z : rotation de la caméra vers le haut
- Q : rotation de la caméra vers la gauche
- D : rotation de la caméra vers la droite
- '+' et '-' : zoom avant et arrière
- Pour se déplacer dans l'espace, utilisez les flèches du clavier

__Lumière__
- L : déplacer le soleil

__Compilation__ 
- make visu

__Execution__
- ./bin/visu *configuration*
