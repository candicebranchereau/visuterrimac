#include <stdlib.h>
#include <math.h>
#include <stdio.h>
  
#include "lodepng.h"

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "visu.h"
#include "scene.h"
#include "draw.h"
#include "geometry.h"

#define TAILLE_MAX 50

// variables globales pour la gestion de la caméra (rotation)
float theta = -M_PI/4.;
float phi = -M_PI/2.0;

float thetalight = M_PI/2; //angle du soleil

Scene scene;
const char* config_name; // fichier de configuration
bool fill = false; //mode filaire

/*********************************************************/
/* fonction de dessin de la scène à l'écran              */
static void drawFunc(void) { 
	/* reinitialisation des buffers : couleur et ZBuffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* modification de la matrice de la scène */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Debut du dessin de la scène
	glPushMatrix();
		
	// mouvement de la caméra
	moveCamera(&scene.camera, theta, phi);

	// mouvement du soleil
	moveLight(&scene.sun, thetalight);	
	
	glColor3f(1.0,1.0,1.0);

	drawScene(scene, fill);

	glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

	/* Fin du dessin */
	glPopMatrix();

	/* fin de la définition de la scène */
	glFinish();

	/* changement de buffer d'affichage */
	glutSwapBuffers();
}

/*********************************************************/
/* fonction de changement de dimension de la fenetre     */
/* paramètres :                                          */
/* - width : largeur (x) de la zone de visualisation     */
/* - height : hauteur (y) de la zone de visualisation    */
static void reshapeFunc(int width,int height) {
	GLfloat  h = (GLfloat) width / (GLfloat) height ;
	
	/* dimension de l'écran GL */
	glViewport(0, 0, (GLint)width, (GLint)height);
	/* construction de la matrice de projection */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* définition de la camera */
	gluPerspective( scene.fov, h, scene.z_near/100.0, scene.z_far);	// Angle de vue, rapport largeur/hauteur, near, far

	/* Retour a la pile de matrice Modelview et effacement de celle-ci */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/*********************************************************/
/* fonction associée aux interruptions clavier           */
/* paramètres :                                          */
/* - c : caractère saisi                                 */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdFunc(unsigned char c, int x, int y) {
	/* sortie du programme si utilisation des touches ESC, */
	switch(c) {
		case 27 :
			exit(0);
			break;
		case 'F' : case 'f' : glPolygonMode(GL_FRONT_AND_BACK,GL_LINE); fill = true;
			break;
		case 'P' : case 'p' : glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); fill = false;
			break;
		case 'Z' : case 'z' : if (phi < 0-10*STEP_ANGLE) phi+=STEP_ANGLE;
			break;
		case 'S' : case 's' : if (phi > -M_PI+10*STEP_ANGLE) phi-=STEP_ANGLE;
			break;
		case 'Q' : case 'q' : theta+=STEP_ANGLE;
			break;
		case 'D' : case 'd' : theta-=STEP_ANGLE;
			break;
		case 'L' : case 'l' : thetalight+=STEP_ANGLE;
			break;
		case '+' : translateFront(&scene.camera, STEP_PROF, theta, phi);
			break;
		case '-' : translateBack(&scene.camera, STEP_PROF, theta, phi);
			break;
		default: 
			printf("Appui sur la touche %c\n",c);
	}
	glutPostRedisplay();
}

/*********************************************************/
/* fonction associée aux interruptions clavier pour les  */
/*          touches spéciales                            */
/* paramètres :                                          */
/* - c : code de la touche saisie                        */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdSpFunc(int c, int x, int y) {
	/* sortie du programme si utilisation des touches ESC, */
	switch(c) {
		case GLUT_KEY_UP :
			translateUp(&scene.camera, STEP);
			break;
		case GLUT_KEY_DOWN :
			translateDown(&scene.camera, STEP);
			break;
		case GLUT_KEY_LEFT :
			translateRight(&scene.camera, STEP);
			break;
		case GLUT_KEY_RIGHT :
			translateLeft(&scene.camera, STEP);
			break;
		default:
			printf("Appui sur une touche spéciale\n");
	}
	glutPostRedisplay();
}

/*********************************************************/
/* fonction d'initialisation des paramètres de rendu et  */
/* des objets de la scènes.                              */
static void init() {
	// Skybox
	Skybox skybox = createSkybox(1.0);

	// Light
    Vector3D poslight = createVector(1, 1, 1);
    ColorRGB colorlight = createColor(0.9, 0.5, 0.);
    Light light = createLight(poslight, colorlight, 0.15);
	thetalight = M_PI/2;

	// Camera
	Vector3D pos = createVector(2,-2,0.4);
    Camera camera = createCamera(pos);
    phi = -M_PI/2.0;
    theta = -M_PI/4;

	// Initialisation de la scene
    scene = createScene(config_name, skybox, light, camera);

	// Ajout arbres
	for (int i = 0; i < 20; i++)
	{
		// position aléatoires sur un point de la map
		int x = rand() % (scene.map.pgm.width);
		int y = rand() % (scene.map.pgm.height);
		Vector3D posTree = createVector(x, y, 0.0);
		Tree tree = createTree(0.2, 0.2, posTree, "arbre");
		addTreeToScene(&scene, tree);
	}

	/* INITIALISATION DES PARAMETRES GL */
	// couleur du fond (gris sombre)
	glClearColor(0.3,0.3,0.3,0.0);
	// activation du ZBuffer
	glEnable( GL_DEPTH_TEST);

	// lissage des couleurs sur les facettes
	glShadeModel(GL_SMOOTH);

}

void idle(void) {
	glutPostRedisplay();
}

int main(int argc, char** argv) {
	// traitement des paramètres du programme propres à GL
	glutInit(&argc, argv);
	if(argc > 1)
	{
		// récupération du fichier de configuration saisi
		printf("config name : %s\n", argv[1]);
		config_name = argv[1];
	}
	
	/* initialisation du mode d'affichage :                */
	/* RVB + ZBuffer + Double buffer.                      */
	glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
	/* placement et dimentions originales de la fenêtre */
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(500, 500);
	// ouverture de la fenêtre
	if (glutCreateWindow("PLIM") == GL_FALSE) {
		return 1;
	}

	init();

	/* association de la fonction callback de redimensionnement */
	glutReshapeFunc(reshapeFunc);
	/* association de la fonction callback d'affichage */
	glutDisplayFunc(drawFunc);
	/* association de la fonction callback d'événement du clavier */
	glutKeyboardFunc(kbdFunc);
	/* association de la fonction callback d'événement du clavier (touches spéciales) */
	glutSpecialFunc(kbdSpFunc);

	glutIdleFunc(idle);

	/* boucle principale de gestion des événements */
	glutMainLoop();

	/* Liberation de la memoire allouee sur le GPU pour la texture */
    glDeleteTextures(1, &scene.skybox.texture);

	/* Cette partie du code n'est jamais atteinte */
	return 0;
}
