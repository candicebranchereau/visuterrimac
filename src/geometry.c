#include <math.h>
#include <stdio.h>

#include "geometry.h"

Point3D createPoint3D(float x, float y, float z) 
{
    Point3D p;
    p.x = x;
    p.y = y;
    p.z = z;
    return p;
}

Vector3D createVector(float x, float y, float z)
{
    Vector3D v;
    v.x = x;
    v.y = y;
    v.z = z;
    return v;
}

RectangleSpace createRectangleSpace(int x1, int y1, int x2, int y2)
{
    RectangleSpace rect;
    rect.x1 = x1;
    rect.y1 = y1;
    rect.x2 = x2;
    rect.y2 = y2;
    return rect;
}

Vector3D createVectorFromPoints(Point3D p1, Point3D p2)
{
    Vector3D v;
    v.x = p2.x - p1.x;
    v.y = p2.y - p1.y;
    v.z = p2.z - p1.z;
    return v;
}

Point3D pointPlusVector(Point3D p, Vector3D v)
{
    v.x += p.x;
    v.y += p.y;
    v.z += p.z;
    return v;
}

Vector3D addVectors(Vector3D v1, Vector3D v2)
{
    v1.x += v2.x;
    v1.y += v2.y;
    v1.z += v2.z;
    return v1;
}

Vector3D subVectors(Vector3D v1, Vector3D v2)
{
    v1.x -= v2.x;
    v1.y -= v2.y;
    v1.z -= v2.z;
    return v1;
}

Vector3D multVector(Vector3D v, float a)
{
    if (a == 0.) {
        v.x = 0.;
        v.y = 0.;
        v.z = 0.;
        return v; 
    }

    v.x *= a;
    v.y *= a;
    v.z *= a;
    return v;
}

Vector3D divVector(Vector3D v, float a)
{
    if (a == 0.) 
        return v;
    v.x /= a;
    v.y /= a;
    v.z /= a;
    return v;
}

float dot(Vector3D v1, Vector3D v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

Vector3D productVector(Vector3D v1, Vector3D v2)
{
    Vector3D v3;
    v3.x = v1.y*v2.z - v1.z*v2.y;
    v3.y = v1.z*v2.x - v1.x*v2.z;
    v3.z = v1.x*v2.y - v1.y*v2.x;
    return v3;
}

float norm(Vector3D v)
{
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

void normalize(Vector3D* v)
{
    float n = norm(*v);
    if (n == 0.)
        return;
    v->x /= n;
    v->y /= n;
    v->z /= n;
}

void printPoint3D(Point3D p)
{
    printf("(%f, %f, %f)", p.x, p.y, p.z);
}

void printVector3D(Vector3D v)
{
    printf("(%f, %f, %f)", v.x, v.y, v.z);
}

Vector3D reflect(Vector3D v, Vector3D n)
{
    Vector3D r = multVector(multVector(subVectors(v,n), 2.0),dot(v,n));
    return r;
}

// calcul de la normale du triangle composé par les points
Vector3D normaleSurface(Vector3D v1, Vector3D v2, Vector3D v3)
{
    Vector3D a = subVectors(v1, v3);
    Vector3D b = subVectors(v1, v2);
    Vector3D normale = productVector(a, b);
    normalize(&normale);

    return normale;
}