#include "draw.h"

#include <stdio.h>

void glDrawRepere(float length, bool fill) {
	// dessin du repère
    if (fill)
    {
        glBegin(GL_LINES);
            glColor3f(1.,0.,0.);
            glVertex3f(0.,0.,0.);
            glVertex3f(length,0.,0.);
            glColor3f(0.,1.,0.);
            glVertex3i(0.,0.,0.);
            glVertex3i(0.,length,0.);
            glColor3f(0.,0.,1.);
            glVertex3i(0.,0.,0.);
            glVertex3i(0.,0.,length);
        glEnd();
    }
}

void drawScene(Scene scene, bool fill)
{
    glDrawRepere(2.0, fill);
    if (!fill) //si on n'est pas en mode fillaire, on dessine
    {
        // skybox à la postion de la caméra
        glPushMatrix();
            glTranslatef(scene.camera.pos.x, scene.camera.pos.y, scene.camera.pos.z);
            drawSkybox(scene, scene.skybox);
        glPopMatrix();

        drawLight(scene.sun);
    }

    glPushMatrix();
        // centrer sur l'origine
        glTranslatef(-scene.map.x_size/2, -scene.map.y_size/2, 0);
        int level = getHeight(&scene.map.quadTree);

        drawMap(scene.map, &scene.map.quadTree, level, scene.camera.pos, scene.sun, fill);

        // boucle pour les arbres
        for (int i=0; i<scene.treeCount; i++)
            drawTree(scene, scene.trees[i]);
    glPopMatrix();
}


void drawMap(Map map, QuadTree* quadTree, int level, Vector3D camera_pos, Light light, bool fill)
{
    int nb_div_x = map.pgm.width - 1;
    int nb_div_y = map.pgm.height - 1;

    float scale_x = map.x_size/(float)nb_div_x;
    float scale_y = map.y_size/(float)nb_div_y;

    // définition des 4 sommets
    Vector3D s0 = createVector(quadTree->vertex[0].x*scale_x, quadTree->vertex[0].y*scale_y, map.z_min + quadTree->vertex[0].z/255.0 * (map.z_max-map.z_min));
    Vector3D s1 = createVector(quadTree->vertex[1].x*scale_x, quadTree->vertex[1].y*scale_y, map.z_min + quadTree->vertex[1].z/255.0 * (map.z_max-map.z_min));
    Vector3D s2 = createVector(quadTree->vertex[2].x*scale_x, quadTree->vertex[2].y*scale_y, map.z_min + quadTree->vertex[2].z/255.0 * (map.z_max-map.z_min));
    Vector3D s3 = createVector(quadTree->vertex[3].x*scale_x, quadTree->vertex[3].y*scale_y, map.z_min + quadTree->vertex[3].z/255.0 * (map.z_max-map.z_min));

    // calcul de la normale de la surface pour l'illumination et le LOD
    Vector3D normaleTriangle1 = normaleSurface(s0, s1, s3);
    Vector3D normaleTriangle2 = normaleSurface(s0, s3, s2);

    // distance entre la caméra et la surface du triangle
    float distance1 = norm(subVectors(camera_pos, normaleTriangle1));
    float distance2 = norm(subVectors(camera_pos, normaleTriangle2));

    // on récupère la distance max
    float distance;
    if (distance1<distance2)
        distance = distance2;
    else
        distance = distance1;

    if (isLeaf(quadTree) || distance > 50/(quadTree->level+1))
    //if (quadTree->level == level)
    {
        if(fill)
        {
            glColor3f(quadTree->level/(level-1), quadTree->level/(level-2), quadTree->level/(level-3));
            glBegin(GL_TRIANGLES);
                glVertex3f(s0.x, s0.y, s0.z);
                glVertex3f(s1.x, s1.y, s1.z);
                glVertex3f(s3.x, s3.y, s3.z);
            glEnd();
            
            glBegin(GL_TRIANGLES);
                glVertex3f(s0.x, s0.y, s0.z);
                glVertex3f(s2.x, s2.y, s2.z);
                glVertex3f(s3.x, s3.y, s3.z);
            glEnd();
        }
        else
        {
            glBegin(GL_TRIANGLES);
                ColorRGB color = createColor(quadTree->vertex[0].z/(255.0*2), 0.01, 0.1);
                ColorRGB colorLambert = modelLambert(color, normaleTriangle1 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s0.x, s0.y, s0.z);
                
                color = createColor(quadTree->vertex[1].z/(255.0*2), 0.01, 0.1);
                colorLambert = modelLambert(color, normaleTriangle1 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s1.x, s1.y, s1.z);
                
                color = createColor(quadTree->vertex[3].z/(255.0*2), 0.01, 0.1);
                colorLambert = modelLambert(color, normaleTriangle1 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s3.x, s3.y, s3.z);
            glEnd();

            glBegin(GL_TRIANGLES);
                color = createColor(quadTree->vertex[0].z/(255.0*2), 0.01, 0.1);
                colorLambert = modelLambert(color, normaleTriangle2 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s0.x, s0.y, s0.z);
                
                color = createColor(quadTree->vertex[2].z/(255.0*2), 0.01, 0.1);
                colorLambert = modelLambert(color, normaleTriangle2 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s2.x, s2.y, s2.z);
                
                color = createColor(quadTree->vertex[3].z/(255.0*2), 0.01, 0.1);
                colorLambert = modelLambert(color, normaleTriangle2 ,light);
                glColor3f(colorLambert.r,colorLambert.g,colorLambert.b); 
                glVertex3f(s3.x, s3.y, s3.z);
            glEnd();
        }
    }
    else
    {
        if(quadTree->topLeft)
            drawMap(map, quadTree->topLeft, level, camera_pos, light, fill);

        if(quadTree->topRight)
            drawMap(map, quadTree->topRight, level, camera_pos, light, fill);

        if(quadTree->bottomLeft)
            drawMap(map, quadTree->bottomLeft, level, camera_pos, light, fill);

        if(quadTree->bottomRight)
            drawMap(map, quadTree->bottomRight, level, camera_pos, light, fill);
    }
}


void drawTree(Scene scene, Tree tree)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tree.texture);
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    float scale_x = scene.map.x_size/(float)(scene.map.pgm.width - 1);
    float scale_y = scene.map.y_size/(float)(scene.map.pgm.height - 1);

    // récupération de la hauteur du terrain à la position de l'arbre
    tree.pos.z = scene.map.z_min + (scene.map.pgm.pixel[(int)tree.pos.x][(int)tree.pos.y]/255.0)*(scene.map.z_max-scene.map.z_min);
    
    glPushMatrix();

        // position du bilboard
        glTranslatef(tree.pos.x*scale_x, tree.pos.y*scale_y, tree.pos.z);
        // rotation en fonction de la rotation de la caméra
        glRotatef(scene.camera.angle*(360/(2*M_PI)), 0., 0., 1.);

        glColor3f(1, 1, 1);

        glBegin(GL_QUADS);
            
            glTexCoord2f(0, 0);
            glVertex3f(0, -tree.size_x/2, tree.size_y);

            glTexCoord2f(1, 0);
            glVertex3f(0, tree.size_x/2, tree.size_y);

            glTexCoord2f(1, 1);
            glVertex3f(0, tree.size_x/2, 0);

            glTexCoord2f(0, 1);
            glVertex3f(0, -tree.size_x/2, 0);

        glEnd();

    glPopMatrix();
}

void drawLight(Light light) 
{
    GLUquadric *quadric = gluNewQuadric();
    glPushMatrix();
        glColor3f(light.color.r, light.color.g, light.color.b);
		glTranslatef(light.pos.x,light.pos.y,light.pos.z);
		gluSphere(quadric,light.radius,16,16);
	glPopMatrix();
}


void drawSkybox(Scene scene, Skybox skybox)
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_CUBE_MAP_ARB); 
    glDepthMask(GL_FALSE);

    glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, skybox.texture);
    glColor3f(1, 1, 1);

    float t = skybox.size;

    glBegin(GL_TRIANGLE_STRIP); // X Négatif   
        glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t); 
        glTexCoord3f(-t,-t,t); glVertex3f(-t,t,-t);
        glTexCoord3f(-t,t,-t); glVertex3f(-t,-t,t);
        glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP); // X Positif
        glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
        glTexCoord3f(t,t,-t); glVertex3f(t,-t,t);
        glTexCoord3f(t,-t,t); glVertex3f(t,t,-t); 
        glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP); // Y Négatif   
        glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
        glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
        glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);
        glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);

    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP); // Y Positif        
        glTexCoord3f(t,t,-t); glVertex3f(-t,t,t);
        glTexCoord3f(-t,t,-t); glVertex3f(t,t,t);  
        glTexCoord3f(t,t,t); glVertex3f(-t,t,-t);
        glTexCoord3f(-t,t,t); glVertex3f(t,t,-t); 
   
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP); // Z Négatif        
        glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);
        glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
        glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
        glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP); // Z Positif    
        glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
        glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
        glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
        glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
    glEnd();

	glDepthMask(GL_TRUE);
    glDisable(GL_TEXTURE_CUBE_MAP_ARB);
}


