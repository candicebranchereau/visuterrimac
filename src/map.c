#include "map.h"

Map createMap(const char *file_name, int x_size, int y_size, int z_min, int z_max)
{
    Map newMap;

    newMap.pgm = getPGMFromFile(file_name);
    newMap.x_size = x_size;
    newMap.y_size = y_size;
    newMap.z_min = z_min;
    newMap.z_max = z_max;

    // Initialisation du quadTree avec les données de la map
    ColorRGB color = createColor(1, 1, 1);
    RectangleSpace space = createRectangleSpace(0, 0, newMap.pgm.width-1, newMap.pgm.height-1);
    newMap.quadTree = initQuadTree(space, 0, color);
    subdiviseSpace(&newMap.quadTree, &newMap.pgm);
  
    return newMap;
}

