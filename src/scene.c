#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scene.h"

#define TAILLE_MAX 50

Scene createScene(const char *config_name, Skybox skybox, Light light, Camera camera)
{
    Scene newScene;
    newScene.treeCount = 0;
    readFileData(config_name, &newScene);
    newScene.skybox = skybox;
    newScene.sun = light;
    newScene.camera = camera;

    return newScene;
}

void readFileData(const char *config_name, Scene *scene) {
	FILE* config_file = NULL;

    char file_name[TAILLE_MAX]; //nom du fichier pgm
    int x_size = 0; //longueur totale du terrain
    int y_size = 0; //largeur totale du terrain
    int z_min = 0; //hauteur minimale du terrain
    int z_max = 0; //hauteur maximale du terrain
    int z_near = 0; //plan eloigné de la caméra
    int z_far = 0; //plan proche de la caméra
    int fov = 0; //angle visible par la camera

    char file[TAILLE_MAX] = "";
    snprintf(file, sizeof file, "doc/%s.timac", config_name);
    config_file = fopen(file, "r+");

    if (config_file == NULL) {
        perror("cannot read configuration file");
        exit(EXIT_FAILURE);
    }

    fscanf(config_file, "%s %d %d %d %d %d %d %d", file_name, &x_size, &y_size, &z_min, &z_max, &z_near, &z_far, &fov);
    printf("info config : %s %d %d %d %d %d %d %d\n", file_name, x_size, y_size, z_min, z_max, z_near, z_far, fov);

    fclose(config_file);

	scene->map = createMap(file_name, x_size, y_size, z_min, z_max);

    scene->z_far = z_far;
    scene->z_near = z_near;
    scene->fov = fov;
}

Tree createTree(float size_x, float size_y, Point3D pos, char* texture_file_name)
{
    Tree tree;
    tree.size_x = size_x;
    tree.size_y = size_y;
    tree.pos = pos;

    // Chargement de l'image
    unsigned char* imagePixels;
    unsigned imageW;
    unsigned imageH;
	char image_path[30] = "";
    snprintf(image_path, sizeof image_path, "doc/%s.png", texture_file_name);

	if(lodepng_decode32_file(&imagePixels, &imageW, &imageH, image_path)) {
        fprintf(stderr, "Echec du chargement de l'image %s\n", image_path);
        exit(EXIT_FAILURE);
    }

	// Initialisation de la texture
    glGenTextures(1, &tree.texture);
    glBindTexture(GL_TEXTURE_2D, tree.texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageW, imageH, 0, GL_RGBA, GL_UNSIGNED_BYTE, imagePixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    return tree;
}

void addTreeToScene(Scene* scene, Tree tree)
{
    if (scene->treeCount >= MAX_TREE_COUNT) {
        printf("Scene tree list is full\n");
    }
    else {
        scene->trees[scene->treeCount++] = tree;
    }
}

Light createLight(Vector3D pos, ColorRGB color, float radius)
{
    Light newLight;
    newLight.pos = pos;
    normalize(&newLight.pos);
    newLight.color = color;
    newLight.radius = radius;

    return newLight;
}

void moveLight(Light *light, float thetalight)
{
    light->pos = createVector(3*cos(thetalight), 0, 3*sin(thetalight));
}

ColorRGB modelLambert(ColorRGB color, Vector3D normale, Light sun)
{
    // calcul en fonction de l'angle entre la normale et le soleil
    float angle = dot(normale, sun.pos);
    color = multColor(color, angle);

    return color;
}

Skybox createSkybox(float size)
{
    Skybox newSkybox;
    newSkybox.size = size;

    // Liste des faces successives pour la création des textures de CubeMap
    GLenum cube_map_target[6] = {           
        GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,
        GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB
    };

    // Chargement de l'image
    unsigned char* imagePixels[6];
    unsigned imageW;
    unsigned imageH;
    const char* image_path[6] = {
        "./doc/left.png",
        "./doc/right.png",
        "./doc/front.png",
        "./doc/back.png",
        "./doc/bottom.png",
        "./doc/top.png"
    };

    for (int i = 0; i < 6; i++) {
        if (lodepng_decode32_file(&imagePixels[i], &imageW, &imageH, image_path[i])) {
            fprintf(stderr, "Echec du chargement de l'image %s\n", image_path[i]);
            exit(EXIT_FAILURE);
        }
    }
    
    // Initialisation de la texture
    glGenTextures(1, &newSkybox.texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, newSkybox.texture);

    for (int i = 0; i < 6; i++)
    {
        glTexImage2D(cube_map_target[i], 0, GL_RGB, imageW, imageH, 0, GL_RGBA, GL_UNSIGNED_BYTE, imagePixels[i]);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);

    return newSkybox;
}


