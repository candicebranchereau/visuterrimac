#include "quadTree.h"

QuadTree initQuadTree(RectangleSpace boundaries, int level, ColorRGB color)
{
    QuadTree node;
    //initialisation de l'espace (racine)
    node.boundaries.x1 = boundaries.x1;
    node.boundaries.y1 = boundaries.y1;
    node.boundaries.x2 = boundaries.x2;
    node.boundaries.y2 = boundaries.y2;

    node.bottomLeft = NULL;
    node.bottomRight = NULL;
    node.topLeft = NULL;
    node.topRight = NULL;

    node.level = level+1;
    //ColorRGB newColor = addColors(color, createColor(1,0,0));
    node.color = color;

    return node;
}

void subdiviseSpace(QuadTree* quadTree, PGM* pgm)
{
    int x1 = quadTree->boundaries.x1;
    int y1 = quadTree->boundaries.y1;
    int x2 = quadTree->boundaries.x2;
    int y2 = quadTree->boundaries.y2;

    int center_x = ceilf((x2 - x1)/2);
    int center_y = ceilf((y2 - y1)/2);

    if(x2-x1 >= 2 && y2-y1 >= 2)   // si on peut encore diviser l'espace
    {
        // TopLeft
        RectangleSpace topLeftBoundaries = createRectangleSpace(x1, y1+center_y, x1+center_x, y2);
        quadTree->topLeft = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->topLeft = initQuadTree(topLeftBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->topLeft, pgm);

        // TopRight
        RectangleSpace topRightBoundaries = createRectangleSpace(x1+center_x, y1+center_y, x2, y2);
        quadTree->topRight = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->topRight = initQuadTree(topRightBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->topRight, pgm);

        // BottomLeft
        RectangleSpace bottomLeftBoundaries = createRectangleSpace(x1, y1, x1+center_x, y1+center_y);
        quadTree->bottomLeft = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->bottomLeft = initQuadTree(bottomLeftBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->bottomLeft, pgm);

        // BottomRight
        RectangleSpace bottomRightBoundaries = createRectangleSpace(x1+center_x, y1, x2, y1+center_y);
        quadTree->bottomRight = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->bottomRight = initQuadTree(bottomRightBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->bottomRight, pgm);

        
    }
    else if(x2-x1 == 2 && y2-y1 == 1) // si on peut diviser que en x
    {
        // BottomLeft
        RectangleSpace bottomLeftBoundaries = createRectangleSpace(x1, y1, x1+1, y2);
        quadTree->bottomLeft = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->bottomLeft = initQuadTree(bottomLeftBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->bottomLeft, pgm);

        // BottomRight
        RectangleSpace bottomRightBoundaries = createRectangleSpace(x1+1, y1, x2, y2);
        quadTree->bottomRight = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->bottomRight = initQuadTree(bottomRightBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->bottomRight, pgm);
    }
    else if(x2-x1 == 1 && y2-y1 == 2) // si on peut diviser que en y
    {
        // TopLeft
        RectangleSpace topLeftBoundaries = createRectangleSpace(x1, y1+1, x2, y2);
        quadTree->topLeft = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->topLeft = initQuadTree(topLeftBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->topLeft, pgm);

        // BottomLeft
        RectangleSpace bottomLeftBoundaries = createRectangleSpace(x1, y1, x2, y1+1);
        quadTree->bottomLeft = (QuadTree*) malloc (sizeof(QuadTree));
        *quadTree->bottomLeft = initQuadTree(bottomLeftBoundaries, quadTree->level, quadTree->color);
        subdiviseSpace(quadTree->bottomLeft, pgm);
    }

    quadTree->vertex[0].x = x1;
    quadTree->vertex[1].x = x2;
    quadTree->vertex[2].x = x1;
    quadTree->vertex[3].x = x2;

    quadTree->vertex[0].y = y2;
    quadTree->vertex[1].y = y2;
    quadTree->vertex[2].y = y1;
    quadTree->vertex[3].y = y1;

    quadTree->vertex[0].z = pgm->pixel[x1][y2];
    quadTree->vertex[1].z = pgm->pixel[x2][y2];
    quadTree->vertex[2].z = pgm->pixel[x1][y1];
    quadTree->vertex[3].z = pgm->pixel[x2][y1];
}

bool isLeaf(QuadTree* quadTree)
{ 
    if (quadTree && quadTree->topLeft == NULL && quadTree->topRight == NULL && quadTree->bottomLeft == NULL && quadTree->bottomRight == NULL)
        return true;

    return false;
}

// calcul la profondeur du quadTree
int getHeight(QuadTree* quadTree)
{
    if (isLeaf(quadTree))
        return quadTree->level;
    else
        getHeight(quadTree->topLeft);
}