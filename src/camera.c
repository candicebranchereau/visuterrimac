#include "camera.h"

Camera createCamera(Vector3D pos)
{
    Camera camera;
    camera.pos = pos;
    camera.up = createVector(0,0,1);

    return camera;
}

void moveCamera(Camera* camera, float theta, float phi)
{
    // mise à jour de l'angle pour la rotation des bilboards
    camera->angle = theta;

    // Direction de regard
	Vector3D forward = createVector(cos(theta)*sin(phi), sin(theta)*sin(phi), cos(phi));
	// Point visé
	camera->target = addVectors(camera->pos,forward);
	// Vecteur left
	Vector3D left = createVector(cos(theta + M_PI/2), sin(theta + M_PI/2), 0);
	normalize(&left);

    // placement de la caméra
	gluLookAt(camera->pos.x, camera->pos.y, camera->pos.z,
              camera->target.x, camera->target.y, camera->target.z,
              camera->up.x, camera->up.y, camera->up.z);
}

// déplacement gauche
void translateLeft(Camera* camera, float step)
{
    Vector3D left = createVector(cos(camera->angle + M_PI/2), sin(camera->angle + M_PI/2), 0);
	normalize(&left);
    camera->pos = addVectors(camera->pos, multVector(left,step));
}

// déplacement droit
void translateRight(Camera* camera, float step)
{
    Vector3D left = createVector(cos(camera->angle + M_PI/2), sin(camera->angle + M_PI/2), 0);
	normalize(&left);
    camera->pos = subVectors(camera->pos, multVector(left,step));
}

// déplacement vers le haut
void translateUp(Camera* camera, float step)
{
    camera->pos = addVectors(camera->pos, multVector(camera->up,step));
}

// déplacement vers le bas
void translateDown(Camera* camera, float step)
{
    camera->pos = subVectors(camera->pos, multVector(camera->up,step));
}

// déplacement en avant
void translateFront(Camera* camera, float step, float theta, float phi)
{
    Vector3D forward = createVector(cos(theta)*sin(phi), sin(theta)*sin(phi), cos(phi));
    camera->pos = addVectors(camera->pos, multVector(forward,step));
}

// déplacement en arrière
void translateBack(Camera* camera, float step, float theta, float phi)
{
    Vector3D forward = createVector(cos(theta)*sin(phi), sin(theta)*sin(phi), cos(phi));
    camera->pos = subVectors(camera->pos, multVector(forward,step));
}