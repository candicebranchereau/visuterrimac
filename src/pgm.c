#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pgm.h"

#define TAILLE_MAX 50

PGM getPGMFromFile(const char *file_name)
{
    PGM newPgm;
    FILE *pgm_file;
    char format[3];

    // Ouverture du fichier nommé file_name
    char file[TAILLE_MAX] = "";
    snprintf(file, sizeof file, "doc/%s.pgm", file_name);
    pgm_file = fopen(file, "rb");
    if (pgm_file == NULL) {
        perror("cannot read pgm file");
        exit(EXIT_FAILURE);
    }

    // test du format (P2 = PGM en ASCII)
    fgets(format, sizeof(format), pgm_file);
    if (strcmp(format, "P2") != 0) {
        perror("wrong file version");
        exit(EXIT_FAILURE);
    }

    // récupération des dimension de l'image et de la valeur maximale des niveaux de gris
    fscanf(pgm_file, "%d %d %d", &newPgm.width, &newPgm.height, &newPgm.max_gray);
    printf("info pgm : %s %d %d %d\n", format, newPgm.width, newPgm.height, newPgm.max_gray);

    // allocation dynamique du tableau de pixel
    newPgm.pixel = (int **)malloc(sizeof(int *) * newPgm.height);
    if (newPgm.pixel == NULL) {
        perror("memory allocation failure");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < newPgm.width; i++) {
        newPgm.pixel[i] = (int *)malloc(sizeof(int) * newPgm.width);
        if (newPgm.pixel[i] == NULL) {
            perror("memory allocation failure");
            exit(EXIT_FAILURE);
        }
    }

    // ajout des valeurs dans le tableau de pixel
    if (newPgm.max_gray == 255) {
        for (int i = 0; i < newPgm.height; i++) {
            for (int j = 0; j < newPgm.width; j++) {
                fscanf(pgm_file, "%d", &newPgm.pixel[i][j]);
            }
        }
    }
 
    fclose(pgm_file);

    return newPgm;
}
